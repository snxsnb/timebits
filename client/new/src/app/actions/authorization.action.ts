import { ActionWithPayload } from '../interfaces/action-with-payload';
import { IUser } from '../interfaces/user';
import _ from 'lodash';

export const AuthorizationActionType = {
    SIGN_UP: 'SIGN_UP',
    SIGNED_IN: 'SIGNED_IN'
};

export interface AuthorizationPayload extends IUser {
    isSignUpNeeded?: boolean;
};

export function signedIn(payload: AuthorizationPayload): ActionWithPayload<AuthorizationPayload> {
    payload.isSignUpNeeded = false;
    return {
        type: AuthorizationActionType.SIGNED_IN,
        payload: payload
    };
};

export function signUp(payload: AuthorizationPayload): ActionWithPayload<AuthorizationPayload> {
    payload.isSignUpNeeded = true;    
    return {
        type: AuthorizationActionType.SIGN_UP,
        payload: payload
    };
};