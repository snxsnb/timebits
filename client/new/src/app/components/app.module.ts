import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { FacebookModule } from 'ngx-facebook';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module'
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import authorizationReducer from '../reducers/authorization.reducer';
import { FbAuthService } from '../services/fb-auth.service';
import { AuthService } from '../services/auth.service';
import { AuthGuard } from '../services/auth-guard.service';
import { BaseInterceptor } from '../interceptors/base.interceptor';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SessionService } from '../services/session.service';

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        StoreModule.forRoot({ authorizationReducer }),
        StoreDevtoolsModule.instrument({ maxAge: 5 }),
        FacebookModule.forRoot()
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        AboutComponent,
        DashboardComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BaseInterceptor,
            multi: true
        },
        SessionService,
        FbAuthService,
        AuthService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
