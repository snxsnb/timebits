import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public userName: string;
    constructor(private authService: AuthService, private store: Store<any>) { }

    ngOnInit() {
        this.store.select('authorizationReducer').subscribe(value => this.userName = value.userName);
    }

    public onLogOut(): void {
        this.authService.signOut();
    }
}
