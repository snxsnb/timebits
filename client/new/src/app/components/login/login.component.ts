import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { FbAuthService } from '../../services/fb-auth.service';
import { IUser } from '../../interfaces/user';

const SIGN_IN_TEXT: string = 'sign in';
const SIGN_UP_TEXT: string = 'sign up';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [AuthService, FbAuthService]
})
export class LoginComponent implements OnInit {
    public readonly SocialKeys = {
        FB: 'fb',
    };

    public username: string;
    public showSocialButtons: boolean;
    public buttonText: string;
    public buttonAction: Function;
    public socialStatus: object;
    public user: IUser;

    constructor(private store: Store<any>, private fbAuthService: FbAuthService, private authService: AuthService) {
        this.socialStatus = {};
        this.user = {};
    }

    async ngOnInit() {
        this.buttonText = SIGN_IN_TEXT;
        this.buttonAction = this.onSignIn;

        this.store.select('authorizationReducer')
            .subscribe(user => {
                this.user = user;
                if (user.isSignUpNeeded) {
                    this.buttonText = SIGN_UP_TEXT;
                    this.buttonAction = this.onSignUp;
                    this.showSocialButtons = false;
                }
            });

        this.socialStatus[this.SocialKeys.FB] = await this.fbAuthService.checkAuthorization();
    }

    onSignIn() {
        this.showSocialButtons = true;
    }

    onSignUp() {
        this.authService.authorize({ ...this.user, userName: this.username });
    }

    async onSocialAuth(socialKey: string) {
        let socialUser;

        if (socialKey === this.SocialKeys.FB) {
            socialUser = this.socialStatus[this.SocialKeys.FB] || await this.fbAuthService.login();
        }

        this.authService.authorize(socialUser);
    }
}
