import { IUser } from './user';

export interface SocialAuthService {
    checkAuthorization(): Promise<IUser>;
    login(): Promise<IUser>;
    logout(): Promise<boolean>;
}