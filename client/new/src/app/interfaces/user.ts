export interface IUser {
    id?: string;
    userName?: string;
    facebookId?: string;
    location?: string;
}