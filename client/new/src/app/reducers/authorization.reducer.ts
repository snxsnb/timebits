import { ActionWithPayload } from '../interfaces/action-with-payload';
import { AuthorizationActionType, AuthorizationPayload } from '../actions/authorization.action';

export default (state = {}, action: ActionWithPayload<AuthorizationPayload>) => {
    switch (action.type) {
        case AuthorizationActionType.SIGNED_IN:
            return { ...state, ...action.payload };
        case AuthorizationActionType.SIGN_UP:
            return { ...state, ...action.payload };
        default:
            return state;
    };
};