import { Injectable } from '@angular/core';
import { CanActivate, Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionService } from './session.service';

@Injectable()
export class AuthGuard implements CanActivate, Resolve<any> {
    constructor(private sessionService: SessionService, private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (route.routeConfig.path === 'login' && this.sessionService.getProperty('token')) {
            this.router.navigate(['/dashboard']);
        }
    }

    public canActivate(): boolean {
        return this.checkAuth();
    }

    private checkAuth(): boolean {
        if (this.sessionService.getProperty('token')) {
            return true;
        }
        
        this.router.navigate(['/login']);
        return false;
    }
}