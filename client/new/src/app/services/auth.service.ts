import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import { signedIn, signUp } from '../actions/authorization.action'
import { IUser } from '../interfaces/user';
import { SessionService } from './session.service';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
    private readonly API_URL: string = environment.apiUrl;

    constructor(private http: HttpClient,
        private errorHandler: ErrorHandler,
        private router: Router,
        private store: Store<any>,
        private sessionService: SessionService) {
    }

    public async authorize(user: IUser) {
        try {
            await this.http.post(`${this.API_URL}/auth`, user).subscribe((response) => {
                if (response && response['token']) {
                    this.store.dispatch(signedIn(response));
                    this.sessionService.setProperty('token', response['token']);
                    this.router.navigate(['/dashboard']);
                } else {
                    this.store.dispatch(signUp(user));
                }
            });
        } catch (err) {
            console.log(err);
        }
    }

    public signOut() {
        this.sessionService.clearSession();
        this.router.navigate(['/login']);
    }
}