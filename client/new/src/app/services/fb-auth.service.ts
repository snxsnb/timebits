import { Injectable, ErrorHandler } from '@angular/core';
import { FacebookService, InitParams, LoginResponse } from 'ngx-facebook';

import { SocialAuthService } from '../interfaces/social-auth-service';
import { IUser } from '../interfaces/user';
import { environment } from '../../environments/environment';

@Injectable()
export class FbAuthService implements SocialAuthService {
    private readonly FB_AUTHORIZED_STATUS: string = 'connected';

    constructor(private fbService: FacebookService, private errorHandler: ErrorHandler) {
        const fbParams: InitParams = environment.fb;
        fbService.init(fbParams);
    }

    checkAuthorization(): Promise<IUser> {
        return this.fbService.getLoginStatus()
            .then(response => this.retrieveFbUser(response));
    }

    login(): Promise<IUser> {
        return this.fbService.login()
            .then(response => this.retrieveFbUser(response))
            .catch(error => { console.log(error); return {}});
    }

    logout(): Promise<boolean> {
        return this.fbService.logout().then(() => true);
    }

    retrieveFbUser(response: LoginResponse) {
        if (response.status === this.FB_AUTHORIZED_STATUS) {
            return { facebookId: response.authResponse.userID };
        }

        return null;
    }
}