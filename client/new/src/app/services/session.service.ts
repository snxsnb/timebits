import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
    private readonly APP_KEY: string = 'Timebits'
    constructor() { }

    public setProperty(name: string, value: string): void {
        sessionStorage.setItem(`${this.APP_KEY}.${name}`, value);
    };
    
    public getProperty(name: string): string {
        return sessionStorage.getItem(`${this.APP_KEY}.${name}`);
    };

    public clearSession(): void {
        sessionStorage.clear();
    }
}
