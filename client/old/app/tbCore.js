var core = angular.module('tbCore', ['tbAuth']);

core.run(function($rootScope) { });

core.controller('coreController', coreController);
function coreController($scope, $http, DataUtilsService) {
  $scope.currentView = './views/HomeView.html';
}

core.controller('timeScrollerController', function($scope, $http, DataUtilsService, AuthService) {
  $scope.canFetch = AuthService.isAuthorized;
  $scope.dataUtils = DataUtilsService;
  
  InitDayNodes($scope);
  $scope.selectedDate = DataUtilsService.GetJustDate(new Date());
  $scope.changeSelectedDate = function(newDate) { $scope.selectedDate = newDate; };
  
  $scope.timeBits = [];
  $scope.$watch('selectedDate', function() { LoadAgenda($scope, $http); });
  $scope.$watch('canFetch()', function() { LoadAgenda($scope, $http); });
}); 
 
function InitDayNodes(scope) {
  scope.dayNodes = [ ];
  
  for (var i = -2; i <= 5; i++) {
    var date = scope.dataUtils.GetJustDate(new Date());
    date.setDate(date.getDate() + i);
    
    var dayNode = {
      Date: date
    };
    
    scope.dayNodes.push(dayNode);
  }
}

function LoadAgenda(scope, http) {
  if (scope.canFetch()) {
    http.get('/api/getAgenda/' + ConvertToSqlDate(scope.selectedDate))
    		.success(function (data) {
      scope.timeBits = [];
      data.forEach(function (element) {
        var tbModel = {
          Name: element.Name,
          Period: scope.dataUtils.GetPeriodStringFromSqlDates(element.StartDate, element.EndDate),
          Type: element.Type,
          Duration: scope.dataUtils.GetHoursFromMilliseconds(scope.dataUtils.ConvertFromSqlDate(element.EndDate).getTime() - scope.dataUtils.ConvertFromSqlDate(element.StartDate).getTime())
        };
        
        scope.timeBits.push(tbModel);
      });
    })
      .error(function (data) {
      console.log('Error: ' + data);
    });
  }
}
 
core.service('DataUtilsService', function() {
  this.GetMonthNameByDate = function (d) {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    return month[d.getMonth()];;
  };
  
  this.GetJustDate = function(d) {
    d.setHours(0,0,0,0);
    return d;
  };
  
  this.GetPeriodStringFromSqlDates = function (start, end) {
    return start.substring(11, 16) + ' - ' + end.substring(11, 16);
  };
  
  this.ConvertFromSqlDate = function (sqlDate) {
    var year = sqlDate.substring(0, 4);
    var month = sqlDate.substring(5, 7);
    var date = sqlDate.substring(8, 10);
    var hours = sqlDate.substring(11, 13);
    var minutes = sqlDate.substring(14, 16);
    return new Date(year, month, date, hours, minutes);
  };
  
  this.GetHoursFromMilliseconds = function (value) {
    return value / 3600000;
  };
});
 
core.factory('authInterceptor', function ($q, $window) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      return config;
    },
    responseError: function (rejection) {
      if (rejection.status === 401) {
      }
      return $q.reject(rejection);
    }
  };
});

core.config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});  

var feedController = function($scope, $http, $interval, AuthService){
    $scope.notifications = [];
    $scope.canFetch = AuthService.isAuthorized;
    
    $scope.refresh = function () {
      if (!$scope.canFetch())
      {
        return;
      }
      
      var lastLoadedDate = new Date();
      lastLoadedDate.setDate(lastLoadedDate.getDate() - 30);
      if ($scope.notifications.length > 0) {
        lastLoadedDate = $scope.notifications[0].DateCreated;  
      }
      
      $http.get('/api/updateFeed/' + ConvertToSqlDate(lastLoadedDate))
    		.success(function(data) {
          console.log(data);
          // data.forEach(function (element) {
          //   var nModel = {
          //     TimeAgo: element.Name,
          //     Message: scope.dataUtils.GetPeriodStringFromSqlDates(element.StartDate, element.EndDate),
          //     Type: element.Type
          //   };
          //   $scope.notifications.push(nModel);
          // });
          data.forEach(function(element) { $scope.notifications.unshift(element); });
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
    };
    
    $interval(function() { $scope.refresh(); }, 30000);
    $scope.$watch('canFetch()', function() { console.log('can fetch changed'); $scope.refresh(); });
};
  
core.controller('feedController', feedController);
  
  
core.controller('eventsController', function ($scope, $http, AuthService, DataUtilsService) {
  $scope.canFetch = AuthService.isAuthorized;
  $scope.dataUtils = DataUtilsService;

  $scope.events = [];
  $scope.locationFilter = '';
  $scope.goingClick = function (eventId) { GoToEvent(eventId, $scope, $http, function () { LoadEvents($scope, $http); }); };

  $scope.$watch('locationFilter', function (newValue) { LoadEvents($scope, $http); });
});

function LoadEvents(scope, http) {
  scope.events = [];
  if (scope.canFetch() && scope.locationFilter) {
    http.get('/api/getEvents/' + scope.locationFilter)
      .success(function (data) {
      data.forEach(function (element) {
        var eventModel = {
          Id: element.id,
          Name: element.Name.substring(0, 20),
          Period: element.StartDate.substring(0, 16).replace('T', ' ') + ' to ' + element.EndDate.substring(0, 16).replace('T', ' '),
          Location: element.Location
        };

        scope.events.push(eventModel);
      });
    })
      .error(function (data) {
      console.log('Error: ' + data);
    });
  }
}
  
  
function GoToEvent(eventId, scope, http, callback) {
  if (scope.canFetch() && eventId) {
    http.post('/api/goToEvent/' + eventId)
      .success(function (data) {
      console.log('callback'); 
      callback();
    })
      .error(function (data) {
      console.log('Error: ' + data);
    });
  }
};
//-------------------------------------------------------------directives region start----------------------------------------------------------
 
 core.directive('tbLeftPanel', function() {
    return {
      restrict: 'E',
      templateUrl: './directives/left-panel.html'
    };
  });
  
core.directive('tbCenterPanel', function() {
    return {
      restrict: 'E',
      templateUrl: './directives/center-panel.html'
    };
  });
  
core.directive('tbRightPanel', function() {
    return {
      restrict: 'E',
      templateUrl: './directives/right-panel.html'
    };
  });
 
 
 core.directive('whenScrollEnds', function() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        $(element).scrollbar({
          "onScroll": function (y, x) {
            if (y.scroll > 0 && y.maxScroll - y.scroll < 50) {
              scope.$apply(attrs.whenScrollEnds);              
            }
          }
        });
      }
    };
  });
  
 core.directive('tbPopUp', function() {
    return {
      templateUrl: './directives/pop-up.html',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });
  
  core.directive('tbSocialItem', function() {
    return {
      restrict: 'E',
      scope: {
        active: '=',
        logout: '&',
        login: '&'
      },
      templateUrl: './directives/social-item.html',
      link: function(scope, element, attrs){
        scope.imageUrl = attrs.image;
        scope.toggle = function() { if(scope.active) scope.logout(); else scope.login(); };
      }    
    };
  });
  
  core.directive('tbTimeScrollerNode', function() {
    return {
      restrict: 'E',
      scope: true,
      templateUrl: './directives/time-scroller-node.html',
      link: function(scope, element, attrs){
        scope.toggle = function() { scope.changeSelectedDate(scope.dayNode.Date); };
      }    
    };
  });
  
  core.directive('tbTimebitItem', function() {
    return {
      restrict: 'E',
      scope: true,
      templateUrl: './directives/timebit-item.html',
      link: function(scope, element, attrs){
        var coreElem = element.children();
        var calculatedHeight = scope.timeBit.Duration * 60;
        coreElem.height(calculatedHeight);
        coreElem.css('line-height', calculatedHeight + 'px');
        
        switch (scope.timeBit.Type) {
          case 'Culture':
            coreElem.css('background-color', '#FC6059');
            break;
            
          case 'Sport':
            coreElem.css('background-color', '#00B7ED');
            break;
            
          case 'SelfEducation':
            coreElem.css('background-color', '#CAE49B');
            break;
            
          case 'Entertainment':
            coreElem.css('background-color', '#7C3F84');
            break;
        
          default:
            coreElem.css('background-color', '#BBBDBC');          
            break;
        }        
      }    
    };
  });
  
  core.directive('tbEventItem', function () {
    return {
      restrict: 'E',
      scope: true,
      templateUrl: './directives/event-item.html',
      link: function (scope, element, attrs) {
        scope.toggle = function() { scope.goingClick(scope.event.Id); };        
      }
    };
  });
  
 //-------------------------------------------------------------directives region end----------------------------------------------------------
 
 function ConvertToSqlDate(date) {
   var typeOfDate = Object.prototype.toString.call(date);
   switch (typeOfDate)
   {
     case '[object Date]':
      return date.toISOString().slice(0, 19).replace('T', ' ');
     
     case '[object String]':
      return date.replace('T', ' ').replace('Z', '');
     
     default:
      return null; 
   }
   
   return null;
 }