var events = angular.module('tbEvents', ['tbAuth']);

events.controller('eventsController', function ($scope, $http, AuthService, DataUtils) {
	$scope.canFetch = AuthService.isAuthorized;
	$scope.dataUtils = DataUtils;

	$scope.events = [];
	$scope.locationFilter = '';

	$scope.$watch('canFetch()', function () { LoadEvents($scope, $http); });
	$scope.$watch('locationFilter', function (newValue) { console.log(newValue); });
});

function LoadEvents(scope, http) {
	if (scope.canFetch()) {
		http.get('/api/getEvents/' + scope.locationFilter)
			.success(function (data) {
				data.forEach(function (element) {
					var eventModel = {
						Name: element.Name,
						Period: scope.dataUtils.GetPeriodStringFromSqlDates(element.StartDate, element.EndDate),
						Location: element.Location
					};

					scope.events.push(eventModel);
				});
			})
			.error(function (data) {
				console.log('Error: ' + data);
			});
	}
}

events.directive('tbEventItem', function () {
	return {
		restrict: 'E',
		scope: true,
		templateUrl: './directives/event-item.html',
		link: function (scope, element, attrs) {
		}
	};
});