var auth = angular.module('tbAuth', ['ngFacebook']);

// Facebook auth
auth.config(function ($facebookProvider) {
  $facebookProvider.setAppId('794376087325725');
  $facebookProvider.setPermissions('email, user_friends, read_custom_friendlists, user_events');
});

auth.run(function ($rootScope) {
  // Load the facebook SDK asynchronously
  (function(){
     // If we've already installed the SDK, we're done
     if (document.getElementById('facebook-jssdk')) { return; }

     // Get the first script element, which we'll use to find the parent node
     var firstScriptElement = document.getElementsByTagName('script')[0];

     // Create a new script element and set its id
     var facebookJS = document.createElement('script'); 
     facebookJS.id = 'facebook-jssdk';

     // Set the new script's source to the source of the Facebook JS SDK
     facebookJS.src = '//connect.facebook.net/en_US/all.js';

     // Insert the Facebook JS SDK into the DOM
     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
   }());
});

auth.service('FbService', function($facebook, $http, $window) {
  var isAuthorized = false;

  function refreshFacebook() {
    $facebook.api("/me").then( 
      function(response) {
        $http.post('/authFacebook', response).success(function(data) { 
            $window.sessionStorage.token = data.token;
            isAuthorized = true;
            console.log('Facebook authorized.');
            apiTest($http, $facebook, response.id);
          });
      },
      function(err) {
        delete $window.sessionStorage.token;
        isAuthorized = false;
        console.log('Facebook authentication failed.');                    
      });
  };
  
  this.refresh = refreshFacebook;
  
  this.signIn = function() {
    $facebook.login().then(function() { refreshFacebook(); });
  };
  
  this.signOut = function() {
    $facebook.logout().then(function() { refreshFacebook(); });
  };
  
  this.isAuthorized = function() { return isAuthorized; };
});

// Main authorization service, encapsulates all social networks 
auth.service('AuthService', function(FbService) {
  this.isAuthorized = function() { return FbService.isAuthorized(); };
  FbService.refresh();
});

auth.controller('authController', function($rootScope, $scope, $facebook, $http, $window, FbService) {
  $scope.fbService = FbService;
});

function apiTest(http, fb, userId) {
  fb.api('/' + userId + '/events?fields=name,place,rsvp_status').then(function (response) {
    // console.log(response.data[0].place.name + ', ' + response.data[0].place.location.city);
    response.data.forEach(function(element) {
      console.log(element);
      http.post('/api/createFbTimeBits', element);
    });
  });
}
