'use strict';
const gulp = require('gulp');
const ts = require('gulp-typescript');

let SERVER_FILES = [
    'server/src/*.js',
    'server/src/**/*.js',
    'server/src/*.json',
    'server/src/**/*.json'
];

const CLIENT_FILES = ['client/new/dist/*.*', 'client/new/dist/**/*.*'];
const SERVER_MODELS = ['server/src/data/interfaces/*.ts'];
const serverTsProject = ts.createProject('server/tsconfig.json');

gulp.task('default', ['copy-client', 'server-scripts', 'server-files']);

gulp.task('copy-client', function () {
    return gulp.src(CLIENT_FILES)
        .pipe(gulp.dest('server/dist/public'));
});

gulp.task('server-files', function() {
    return gulp.src(SERVER_FILES)
        .pipe(gulp.dest('server/dist'));
});

gulp.task('server-scripts', () => {
    return serverTsProject.src()
        .pipe(serverTsProject())
        .js
        .pipe(gulp.dest('server/dist'));
});

gulp.task('copy-server-models', () => {
    return gulp.src(SERVER_MODELS)
        .pipe(gulp.dest('client/new/src/app/interfaces'));
});

gulp.task('watch', () => {
    gulp.watch('server/src/**/*.ts', ['server-scripts']);
    gulp.watch(SERVER_FILES, ['server-files']);
});