import * as express from 'express';
import appConfig from '../config/app.config';
import { UsersRouter } from './users/users-router';
import DbProvider from '../data/db/db-provider';
const expressJwt = require('express-jwt');

class Api {
    public init(app: express.Application, dbProvider: DbProvider) {
        app.use('/api', expressJwt({ secret: appConfig.config.secret }));
        app.use('/api/users', new UsersRouter(dbProvider).router);
    }
}
export default Api;