var sql = require('mssql');
var jwt = require('jsonwebtoken');
var path = require('path');
var localSecret;
	
module.exports = function(router, secret) {
	localSecret = secret;

	router.post('/authFacebook', function(req, res) {
		AuthWithFacebook(req.body, function(token){
			if (token) {
				res.json({ token: token });
			}
			else
			{
				res.status(401).send('Authentication failed.');
			}
		});
	});
		
	router.get('/api/updateFeed/:lastLoadedDate', function(req, res) {
		var userId = GetUserIdFromRequest(req);
		if (userId && req.params.lastLoadedDate) {
			QueryDb(
				"SELECT TOP 10 * FROM [Notification] WHERE [DateCreated] > '" + req.params.lastLoadedDate + "' AND [UserId] = " + userId + " ORDER BY [DateCreated] ASC",
				function(result) { if (result.IsSuccess) res.send(result.Value); });
		}
		else {
			res.status(404).send('Bad request');
		}
	});
	
	router.get('/api/getAgenda/:date', function(req, res) {
		var userId = GetUserIdFromRequest(req);
		if (userId && req.params.date) {
			QueryDb(
				"SELECT [TB].*,\n" +
					"ISNULL([E].[Name], [TB].[Comment]) AS [Name],\n" +
					"[Type].[Title] AS [Type]\n" +
					"FROM [TimeBit] AS [TB]\n" +
						"LEFT JOIN [Event] AS [E] ON [E].[id] = [TB].[EventId]\n" +
						"OUTER APPLY (\n" +
							"SELECT TOP 1 [I].[Title]\n" +
								"FROM [Interest] AS [I]\n" +
									"LEFT JOIN [EventInterest] AS [EI] ON [EI].[InterestId] = [I].[id] AND [EI].[EventId] = [E].[id]\n" +
									"LEFT JOIN [TimeBitInterest] AS [TBI] ON [TBI].[InterestId] = [I].[id] AND [TBI].[TimeBitId] = [TB].[id]\n" +
								"WHERE ISNULL([EI].[id], [TBI].[id]) IS NOT NULL\n" +
						") AS [Type]\n" +
					"WHERE [TB].[StartDate] >= '" + req.params.date + "' AND [TB].[StartDate] <  DATEADD(dd, 1, '" + req.params.date + "') AND [TB].[UserId] = " + userId + "\n" +
					"ORDER BY [StartDate] ASC",
				function(result) { if (result.IsSuccess) res.send(result.Value); });
		}
		else {
			res.status(404).send('Bad request');
		}
	});
	
	router.get('/api/getEvents/:locationFilter', function (req, res) {
		var userId = GetUserIdFromRequest(req);
		if (userId && req.params.locationFilter) {
			QueryDb(
				"SELECT [E].*\n" +
				"FROM [Event] AS [E]\n" +
					"LEFT JOIN [TimeBit] AS [TB] ON [TB].[EventId] = [E].[id] AND [TB].[UserId] = " + userId + "\n" +
				"WHERE ([E].[Location] LIKE '%" + req.params.locationFilter + "%'\n" +
					"OR [E].[Name] LIKE '%" + req.params.locationFilter + "%'\n" +
					"OR [E].[Description] LIKE '%" + req.params.locationFilter + "%')\n" +
					"AND [TB].[id] IS NULL\n" +
				"ORDER BY [StartDate] ASC",
				function (result) { if (result.IsSuccess) res.send(result.Value); });
		}
	});
	
	router.post('/api/goToEvent/:eventId', function(req, res) {
		var userId = GetUserIdFromRequest(req);
		if (userId && req.params.eventId) {
			QueryDb(
				"EXEC sp_CreateBoundTimeBit " + userId + ", NULL, NULL, NULL, 0, NULL, " + req.params.eventId + ", NULL;",
				function (result) {
					if (result.IsSuccess) {
						res.send(result.Value);
					}
				});
		}
		else {
			res.status(404).send('Bad request');
		}
	});
	
	router.post('/api/createFbTimeBits', function(req, res) {
		var userId = GetUserIdFromRequest(req);
		if (userId) {
			CreateFbTimeBit(req.body, userId, function() { res.send('FB event processed.'); });
		}		
	});

	router.get('*', function(req, res) {
		res.sendFile(path.resolve('client/old/app/index.html')); // load the single view file (angular will handle the page changes on the front-end)
	});
};

function QueryDb(sqlQuery, callback)
{
	var result = {
		IsSuccess : false,
		Value : null	
	};
	
	var request = new sql.Request();
	request.query(sqlQuery, function(err, recordset) {
		console.log('SQLQRY:	' + sqlQuery);					
		
		if (err){
	    	console.log('SQLERR:	' + err);
			callback(result);				
	    }
		
		console.log(recordset);
		result.IsSuccess = true;
    	result.Value = recordset;
		callback(result);
	});
}

function AuthWithFacebook(facebookObj, callback) {
	QueryDb("SELECT [id] FROM [User] WHERE [FacebookId] = '" + facebookObj.id + "'", function(result){
		if (result.IsSuccess)
		{
			if (result.Value.length == 1 && result.Value[0].id) {
				callback(GenerateToken(result.Value[0].id));				
			}
			else
			{
				QueryDb(
					"INSERT INTO [User]([FacebookId]) OUTPUT [inserted].[id] VALUES ('"+ facebookObj.id + "')",
					function(result) { if (result.IsSuccess) callback(GenerateToken(result.Value[0].id)); });
			}
		}
	});
}

function CreateFbTimeBit(fbEvent, userId, successCallback) {
	if (fbEvent.rsvp_status != 'attending' && fbEvent.rsvp_status != 'unsure') {
		return;
	}
	
	var location = fbEvent.place.name + fbEvent.place.location === 'undefined' ? ', ' + fbEvent.place.location.city : '';
	var startTime = ConvertFbDateToSql(fbEvent.start_time);
	var endTime = fbEvent.end_time ? "'" + ConvertFbDateToSql(fbEvent.end_time) + "'" : "DATEADD(hh, 1, '" + startTime + "')";
	QueryDb(
		"DECLARE @endTime datetime = " + endTime + "; EXEC sp_CreateBoundTimeBit " + userId + ", N'" + location + "', '" + startTime +"', @endTime, 0, N'" + fbEvent.name + "', NULL, 1;",
		function(result) {
			if (result.IsSuccess)
			{
				successCallback();
				console.log("FB event for user '" + userId + "' was processed.");
			}
	});
	
	// QueryDb("SELECT 1 FROM [TimeBit] WHERE [UserId] = " + userId + " AND [StartDate] < " + endTime + " AND [EndDate] > '" + startTime + "'", function(result){
	// 	if (result.IsSuccess)
	// 	{
	// 		if (result.Value && result.Value.length > 0) {
	// 			console.log('TimeBit is already occupied.');
	// 		}
	// 		else
	// 		{
	// 			QueryDb(
	// 				"INSERT INTO [TimeBit]([Comment], [UserId], [StartDate], [EndDate], [SharingOption], [IsFree]) " +
	// 				"VALUES (N'"+ fbEvent.name + "', " + userId +", '" + startTime +"', " + endTime + ", 0, 0)",
	// 				function(result) {  });
	// 		}
	// 	}
	// });
}


function GenerateToken(tbUserId) {
	var profile = {
		userId: tbUserId
	};
	
	var token = jwt.sign(profile, localSecret, { expiresInMinutes: 60*5 });
	return token;
}

function GetUserIdFromRequest(req) {
	if (req.headers.authorization) {
		var token = req.headers.authorization.split(' ')[1]; 
		var obj = jwt.decode(token);
		
		return obj.userId;	
	}
	
	return null;
}

function ConvertFbDateToSql(date) {
	var utcDate = date.substring(0, 19).replace('T', ' ');
	return utcDate;
}
