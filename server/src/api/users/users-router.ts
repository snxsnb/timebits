import { Router, Request, Response, NextFunction } from 'express';
import DbProvider from '../../data/db/db-provider';

export class UsersRouter {
    public router: Router;
    private dbProvider: DbProvider;

    constructor(dbProvider: DbProvider) {
        this.router = Router();
        this.dbProvider = dbProvider;
        this.init();
    }

    public getAll(req: Request, res: Response, next: NextFunction) {
        this.dbProvider.getAllUsers().then(users => res.status(200).send(users));
    }

    public createUser(req: Request, res: Response, next: NextFunction) {
        this.dbProvider.createUser(req.body).then(createdUser => res.status(200).send(createdUser));
    }

    public init() {
        this.router.get('/', this.getAll.bind(this));
        this.router.post('/', this.createUser.bind(this));
    }
}