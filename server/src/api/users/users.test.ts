import * as mocha from 'mocha';
import * as chai from 'chai';

import appConfig from '../../config/app.config';
import app from '../../app';
import Api from '../index';

const chaiHttp = require('chai-http');
const jwt = require('jsonwebtoken');

const token = jwt.sign('123', appConfig.config.secret);
const AUTH_HEADER = `Bearer ${token}`;

chai.use(chaiHttp);
const expect = chai.expect;

describe('GET api/users', () => {
    it('responds with success status', () => {
        return chai.request(app)
            .get('/api/users')
            .set('Authorization', AUTH_HEADER)
            .end((err, res) => {
                    expect(res.status).to.equal(200);
            });
    });
});