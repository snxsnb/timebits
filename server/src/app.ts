import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import appConfig from './config/app.config';
import DbProvider from './data/db/db-provider';
import MongoProvider from './data/db/mongo/mongo-provider';
import Api from './api';
import Routes from './routes';

class App {
    public express: express.Application;
    private api: Api;
    private routes: Routes;
    private dbProvider: DbProvider;

    constructor () {
        appConfig.setAppConfig(process.env.NODE_ENV);
        this.dbProvider = new MongoProvider();
        this.express = express();
        this.routes = new Routes();
        this.api = new Api();

        this.dbProvider.init();
        this.middleware();
        this.initRoutes();
    }

    private middleware(): void {
        this.express.use(express.static(__dirname + '/public'));
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*'); // TODO: remove on prod
            res.setHeader("Access-Control-Allow-Credentials", "true");
            res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
            next();
        });
    }

    private initRoutes(): void {
        this.routes.init(this.express, this.dbProvider);
        this.api.init(this.express, this.dbProvider);
        
        this.express.use((req, res, next) => {
            res.sendfile(__dirname + '/public/index.html');
        });
    }
}

export default new App().express;