export interface AppProperties {
    secret: string,
    db: {
        host: string,
        port: number,
        dbName: string,
        dbUser: string,
        dbPassword: string
    }
};