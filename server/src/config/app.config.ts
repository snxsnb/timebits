import { AppProperties } from './app-properties.model';

class AppConfig {
    private appConfig;

    public setAppConfig(configName: string) {
        this.appConfig = require(`./${configName}.properties.json`);
    }

    public get config(): AppProperties {
        return this.appConfig;
    }
}

const config = new AppConfig();
export default config;