import { IUser } from '../interfaces/user';

interface DbProvider {
    init(): void;
    getAllUsers(): Promise<IUser[]>;
    createUser(user: IUser): Promise<IUser>;
    findUser(user: IUser): Promise<IUser>;
}
export default DbProvider;