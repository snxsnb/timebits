import { Model } from "mongoose";
import { IUserModel } from "./user";

export interface IDbModel {
    user: Model<IUserModel>;
}