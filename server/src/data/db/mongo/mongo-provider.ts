import appConfig from '../../../config/app.config';
import DbProvider from '../db-provider';
import userSchema from './schemas/user-schema';
import { Connection } from 'mongoose';
import { IDbModel } from './models/db-model';
import { IUserModel } from './models/user';
import { IUser } from '../../interfaces/user';

const mongoose = require('mongoose');

class MongoProvider implements DbProvider {
    private connection: Connection;
    private model: IDbModel;
    private readonly CONNECTION_STRING: string;

    constructor() {
        let dbConfig = appConfig.config.db;
        this.CONNECTION_STRING = `mongodb://${dbConfig.dbUser}:${dbConfig.dbPassword}@${dbConfig.host}:${dbConfig.port}/${dbConfig.dbName}`;
        this.model = <IDbModel>(new Object());
    }

    public init() {
        this.connection = mongoose.createConnection(this.CONNECTION_STRING);
        this.model.user = this.connection.model<IUserModel>('User', userSchema);
    }

    public getAllUsers() {
        return this.model.user.find().then(res => res.map(userModel => userModel.toUser()));
    }

    public createUser(user: IUser) {
        return this.model.user.create(user).then(res => res.toUser());
    }

    public findUser(user: IUser) {
        return this.model.user.findOne(user).then(res => res && res.toUser())
    }
}
export default MongoProvider;