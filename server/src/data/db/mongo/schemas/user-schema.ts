import { Mongoose, Schema } from 'mongoose';
import { IUser } from '../../../interfaces/user';

const userSchema: Schema = new Schema({
    id: String,
    userName: String,
    facebookId: String,
    location: String
});

userSchema.methods.toUser = function () {
    return {
        id: this._id,
        userName: this.userName,
        facebookId: this.facebookId,
        location: this.location
    };
}

export default userSchema;