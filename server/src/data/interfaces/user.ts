const _ = require('lodash');

export interface IUser {
    id?: string;
    userName?: string;
    facebookId?: string;
    location?: string;
}

export const isValidToSignIn = (user: IUser) => !_.isEmpty(user.facebookId);
export const isValidToSignUp = (user: IUser) => !_.isEmpty(user.facebookId) && !_.isEmpty(user.userName);