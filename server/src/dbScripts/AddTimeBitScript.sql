DECLARE @counter int = 0;
DECLARE @today datetime = CAST(GETDATE() AS date);
DECLARE @userID bigint = (SELECT TOP 1 [id] FROM [TB].[dbo].[User]);

WHILE @counter < 45
BEGIN
INSERT INTO [TB].[dbo].[TimeBit]
           ([UserId]
           ,[Location]
           ,[StartDate]
           ,[EndDate]
           ,[SharingOption]
           ,[IsFree]
           ,[Comment]
           ,[EventId])
     VALUES
           (@userID
           ,'Kyiv'
           ,DATEADD(HOUR, 20, DATEADD(DAY, @counter, @today))
           ,DATEADD(HOUR, 23, DATEADD(DAY, @counter, @today))
           ,0
           ,0
           ,'my free time #' + CAST(@counter AS nvarchar)
           ,NULL);
    SET @counter = @counter + 1;
END