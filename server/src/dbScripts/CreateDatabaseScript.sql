IF db_id('TB') IS NOT NULL
	RAISERROR('Database with the same name already exists. Drop it first.', 20, -1) WITH LOG;
	
CREATE DATABASE [TB];
GO

CREATE TABLE [TB].[dbo].[Interest]
(
	[id] int IDENTITY(1, 1),
	[Title] nvarchar(30) NOT NULL,
	CONSTRAINT [PK_Interest_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

CREATE TABLE [TB].[dbo].[User]
(
	[id] bigint IDENTITY(1, 1),
	[FacebookId] nvarchar(20)NOT NULL,
	[Location] nvarchar(256) NULL,
	CONSTRAINT [PK_User_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

CREATE TABLE [TB].[dbo].[Event]
(
	[id] bigint IDENTITY(1, 1),
	[OwnerId] bigint CONSTRAINT [FK_Event_OwnerId_User_id] FOREIGN KEY REFERENCES [User]([id]),
	[Location] nvarchar(256) NULL,
	[Name] nvarchar(128) NOT NULL,
	[Description] nvarchar(MAX) NULL,
	[StartDate] datetime NOT NULL,
	[EndDate] datetime NOT NULL,
	CONSTRAINT [PK_Event_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

CREATE TABLE [TB].[dbo].[EventInterest]
(
	[id] int IDENTITY(1, 1),
	[InterestId] int CONSTRAINT [FK_EventInterest_InterestId_Interest_id] FOREIGN KEY REFERENCES [Interest]([id]),
	[EventId] bigint CONSTRAINT [FK_EventInterest_EventId_Event_id] FOREIGN KEY REFERENCES [Event]([id]),
	CONSTRAINT [PK_EventInterest_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

CREATE TABLE [TB].[dbo].[EventPeer]
(
	[id] int IDENTITY(1, 1),
	[UserId] bigint CONSTRAINT [FK_EventPeer_UserId_User_id] FOREIGN KEY REFERENCES [User]([id]),
	[EventId] bigint CONSTRAINT [FK_EventPeer_EventId_Event_id] FOREIGN KEY REFERENCES [Event]([id]),
	CONSTRAINT [PK_EventPeer_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

--creating central table [TimeBit]
CREATE TABLE [TB].[dbo].[TimeBit]
(
	[id] bigint IDENTITY(1, 1),
	[UserId] bigint CONSTRAINT [FK_TimeBit_UserId_User_id] FOREIGN KEY REFERENCES [User]([id]),
	[Location] nvarchar(256) NULL,
	[StartDate] datetime NOT NULL,
	[EndDate] datetime NOT NULL,
	[SharingOption] tinyint NOT NULL,
	[IsFree] bit NOT NULL,
	[Comment] nvarchar(MAX) NULL,
	[EventId] bigint NULL CONSTRAINT [FK_TimeBit_EventId_Event_id] FOREIGN KEY REFERENCES [Event]([id]),
	CONSTRAINT [PK_TimeBit_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];

CREATE TABLE [TB].[dbo].[TimeBitInterest]
(
	[id] int IDENTITY(1, 1),
	[InterestId] int CONSTRAINT [FK_TimeBitInterest_InterestId_Interest_id] FOREIGN KEY REFERENCES [Interest]([id]),
	[TimeBitId] bigint CONSTRAINT [FK_TimeBitInterest_TimeBitId_TimeBit_id] FOREIGN KEY REFERENCES [TimeBit]([id]),
	CONSTRAINT [PK_TimeBitInterest_id] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY];