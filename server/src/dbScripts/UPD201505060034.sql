CREATE TABLE [Notification]
(
	[id] bigint IDENTITY(1, 1),
	[UserId] bigint CONSTRAINT [FK_Notification_UserId_User_id] FOREIGN KEY REFERENCES [User]([id]),
	[Message] nvarchar(128) NULL,
	[Type] tinyint NOT NULL,
	[Data] nvarchar(128) NULL,
	[DateCreated] datetime NOT NULL,
	CONSTRAINT [PK_Notification] PRIMARY KEY ([id] ASC)
) ON [PRIMARY];