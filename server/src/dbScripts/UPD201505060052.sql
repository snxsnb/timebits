CREATE PROCEDURE sp_CreateBoundTimeBit @userId bigint, @location nvarchar(256), @startDate datetime, @endDate datetime, @sharingOption int, @comment nvarchar(max), @eventId bigint
AS
BEGIN
	DECLARE @foundTimeBitIds TABLE([TimeBitId] bigint, [IsFree] bit);
	INSERT INTO @foundTimeBitIds([TimeBitId], [IsFree]) SELECT [id], [IsFree] FROM [TimeBit] WHERE [UserId] = @userId AND [StartDate] < @endDate AND [EndDate] > @startDate;

	IF ((SELECT [TimeBitId] FROM @foundTimeBitIds WHERE [IsFree] = 0) IS NULL)
	BEGIN
		DELETE FROM [TimeBit] WHERE [id] IN (SELECT [TimeBitId] FROM @foundTimeBitIds);

		INSERT INTO [TimeBit]([UserId], [Location], [StartDate], [EndDate], [SharingOption], [IsFree], [Comment], [EventId])
		VALUES (@userId, @location, @startDate, @endDate, @sharingOption, 0, @comment, @eventId);

		DECLARE @newTimeBitId bigint = (SELECT SCOPE_IDENTITY());

		DECLARE @notifymessage nvarchar(128);
		SET @notifymessage = (SELECT CASE WHEN @eventId IS NULL THEN @comment ELSE (SELECT [Name] FROM [Event] WHERE [id] = @eventId) END);

		INSERT INTO [Notification]([UserId], [Message], [Type], [Data], [DateCreated])
		VALUES (@userId, @notifymessage, 1 /*stands for new TimeBit*/, @newTimeBitId, GETDATE());
	END
END