CREATE PROCEDURE [dbo].[sp_CreateFreeTimeBit]
	@userId bigint,
	@location nvarchar(256),
	@startDate datetime,
	@endDate datetime,
	@sharingOption int,
	@comment nvarchar(max),
	@interestId int
AS
BEGIN
	IF ((SELECT TOP 1 [id] FROM [TimeBit] WHERE [UserId] = @userId AND [StartDate] < @endDate AND [EndDate] > @startDate) IS NULL)
	BEGIN
		INSERT INTO [TimeBit]([UserId], [Location], [StartDate], [EndDate], [SharingOption], [IsFree], [Comment])
		VALUES (@userId, @location, @startDate, @endDate, @sharingOption, 1, @comment);

		DECLARE @newTimeBitId bigint = (SELECT SCOPE_IDENTITY());
		INSERT INTO [TimeBitInterest]([InterestId], [TimeBitId])
		VALUES (@interestId, @newTimeBitId);
		RETURN 1;
	END
	
	RETURN 0;
END

ALTER TABLE [dbo].[TimeBitInterest] DROP CONSTRAINT [FK_TimeBitInterest_TimeBitId_TimeBit_id];

ALTER TABLE [dbo].[TimeBitInterest]  WITH CHECK ADD  CONSTRAINT [FK_TimeBitInterest_TimeBitId_TimeBit_id] FOREIGN KEY([TimeBitId])
REFERENCES [dbo].[TimeBit] ([id]) ON DELETE CASCADE;