ALTER PROCEDURE [dbo].[sp_CreateBoundTimeBit]
	@userId bigint,
	@location nvarchar(256),
	@startDate datetime,
	@endDate datetime,
	@sharingOption int,
	@comment nvarchar(max),
	@eventId bigint,
	@interestId int
AS
BEGIN
	DECLARE @foundTimeBitIds TABLE([TimeBitId] bigint, [IsFree] bit);
	INSERT INTO @foundTimeBitIds([TimeBitId], [IsFree])
	SELECT [id], [IsFree]
		FROM [TimeBit]
		WHERE [UserId] = @userId AND [StartDate] < ISNULL(@endDate, (SELECT [EndDate] FROM [Event] WHERE [id] = @eventId))
			AND [EndDate] > ISNULL(@startDate, (SELECT [StartDate] FROM [Event] WHERE [id] = @eventId));

	IF ((SELECT [TimeBitId] FROM @foundTimeBitIds WHERE [IsFree] = 0) IS NULL)
	BEGIN
		DELETE FROM [TimeBit] WHERE [id] IN (SELECT [TimeBitId] FROM @foundTimeBitIds);

		IF (@eventId IS NULL)
		BEGIN
			INSERT INTO [TimeBit]([UserId], [Location], [StartDate], [EndDate], [SharingOption], [IsFree], [Comment], [EventId])
			VALUES (@userId, @location, @startDate, @endDate, @sharingOption, 0, @comment, @eventId);
		END
		ELSE
			INSERT INTO [TimeBit]([UserId], [Location], [StartDate], [EndDate], [SharingOption], [IsFree], [Comment], [EventId])
			SELECT @userId, [E].[Location], [E].[StartDate], [E].[EndDate], @sharingOption, 0, @comment, [E].[id] FROM [Event] AS [E] WHERE [E].[id] = @eventId;

		DECLARE @newTimeBitId bigint = (SELECT SCOPE_IDENTITY());

		DECLARE @notifymessage nvarchar(128);
		SET @notifymessage = (SELECT CASE WHEN @eventId IS NULL THEN @comment ELSE (SELECT [Name] FROM [Event] WHERE [id] = @eventId) END);

		INSERT INTO [Notification]([UserId], [Message], [Type], [Data], [DateCreated])
		VALUES (@userId, @notifymessage, 1 /*stands for new TimeBit*/, @newTimeBitId, GETDATE());

		IF (@eventId IS NULL)
		BEGIN
			INSERT INTO [TimeBitInterest]([InterestId], [TimeBitId])
			VALUES (ISNULL(@interestId, (SELECT TOP 1 [id] FROM [Interest])), @newTimeBitId);
		END
	END
END