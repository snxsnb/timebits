import * as http from 'http';
import app from './app';

const port = process.env.PORT || 1337;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);
server.on('listening', onListening);

function onListening():void {
    let address = server.address();
    console.log(`Listening on ${address.port}`);
}