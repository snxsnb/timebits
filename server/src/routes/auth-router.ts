import { Router, Request, Response, NextFunction } from 'express';
import { sign } from 'jsonwebtoken';
import _ from 'lodash';

import DbProvider from '../data/db/db-provider';
import { IUser, isValidToSignIn, isValidToSignUp } from '../data/interfaces/user';
import appConfig from '../config/app.config';

export class AuthRouter {
    public router: Router;
    private dbProvider: DbProvider;

    constructor(dbProvider: DbProvider) {
        this.router = Router();
        this.dbProvider = dbProvider;
        this.init();
    }

    public authorizeUser(req: Request, res: Response, next: NextFunction) {
        const requestedUser = req.body as IUser;
        if (isValidToSignIn(requestedUser)) {
          this.dbProvider.findUser(requestedUser).then(user => {
                if (user) {
                    res.status(200).send({ ...this.generateToken(user), ...user });
                } else if (isValidToSignUp(requestedUser)) {
                    this.dbProvider.createUser(req.body).then(user => {
                        res.status(200).send({ ...this.generateToken(user), ...user });
                    });
                } else {
                    res.status(200).send({}); // TODO: refactor 
                }
            });
        } else {
          res.status(401).send();
        }
    }

    public generateToken(user: IUser) {
        return { token: sign(user.facebookId, appConfig.config.secret) };
    }

    public init() {
        this.router.post('/', this.authorizeUser.bind(this));
    }
}