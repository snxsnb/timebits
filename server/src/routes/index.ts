import * as express from 'express';
import DbProvider from '../data/db/db-provider';
import { AuthRouter } from './auth-router';

class Routes {
    public init(app: express.Application, dbProvider: DbProvider) {
        app.use('/auth', new AuthRouter(dbProvider).router);
    }
}
export default Routes;